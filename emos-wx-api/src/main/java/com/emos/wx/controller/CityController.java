package com.emos.wx.controller;

import com.emos.wx.bean.HelloForm;
import com.emos.wx.utils.R;
import com.emos.wx.utils.ResultCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@Api(tags = "用户管理相关接口")
public class CityController {

    @ApiOperation(value = "测试接口")
    @GetMapping("/hello")
    public Map hello(){
        return R.ok("sucess!");
    }

    @ApiOperation(value = "say接口测试")
    @GetMapping("/say")
    public Map say(@Valid HelloForm helloForm){
        throw new RuntimeException("test galobal exception");
    }
}
