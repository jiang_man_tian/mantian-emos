package com.emos.wx.dao;

import com.emos.wx.bean.TbFaceModel;
import org.springframework.stereotype.Repository;

@Repository
public interface TbFaceModelDao {

    int deleteByPrimaryKey(Integer id);

    int insert(TbFaceModel record);

    int insertSelective(TbFaceModel record);

    TbFaceModel selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbFaceModel record);

    int updateByPrimaryKey(TbFaceModel record);
}