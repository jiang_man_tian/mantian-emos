package com.emos.wx.dao;

import com.emos.wx.bean.QrtzFiredTriggers;
import org.springframework.stereotype.Repository;

@Repository
public interface QrtzFiredTriggersDao {

    int deleteByPrimaryKey(Integer id);

    int insert(QrtzFiredTriggers record);

    int insertSelective(QrtzFiredTriggers record);

    QrtzFiredTriggers selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(QrtzFiredTriggers record);

    int updateByPrimaryKey(QrtzFiredTriggers record);
}