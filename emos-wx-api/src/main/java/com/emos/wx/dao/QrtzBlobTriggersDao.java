package com.emos.wx.dao;

import com.emos.wx.bean.QrtzBlobTriggers;
import org.springframework.stereotype.Repository;

@Repository
public interface QrtzBlobTriggersDao {

    int deleteByPrimaryKey(Integer id);

    int insert(QrtzBlobTriggers record);

    int insertSelective(QrtzBlobTriggers record);

    QrtzBlobTriggers selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(QrtzBlobTriggers record);

    int updateByPrimaryKey(QrtzBlobTriggers record);
}