package com.emos.wx.dao;

import com.emos.wx.bean.TbHolidays;
import org.springframework.stereotype.Repository;

@Repository
public interface TbHolidaysDao {

    int deleteByPrimaryKey(Integer id);

    int insert(TbHolidays record);

    int insertSelective(TbHolidays record);

    TbHolidays selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbHolidays record);

    int updateByPrimaryKey(TbHolidays record);
}