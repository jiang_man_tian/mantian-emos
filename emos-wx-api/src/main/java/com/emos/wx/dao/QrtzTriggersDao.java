package com.emos.wx.dao;

import com.emos.wx.bean.QrtzTriggers;
import org.springframework.stereotype.Repository;

@Repository
public interface QrtzTriggersDao {

    int deleteByPrimaryKey(Integer id);

    int insert(QrtzTriggers record);

    int insertSelective(QrtzTriggers record);

    QrtzTriggers selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(QrtzTriggers record);

    int updateByPrimaryKey(QrtzTriggers record);
}