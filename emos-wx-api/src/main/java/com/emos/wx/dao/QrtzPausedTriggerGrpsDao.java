package com.emos.wx.dao;

import com.emos.wx.bean.QrtzPausedTriggerGrps;
import org.springframework.stereotype.Repository;

@Repository
public interface QrtzPausedTriggerGrpsDao {

    int deleteByPrimaryKey(Integer id);

    int insert(QrtzPausedTriggerGrps record);

    int insertSelective(QrtzPausedTriggerGrps record);

    QrtzPausedTriggerGrps selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(QrtzPausedTriggerGrps record);

    int updateByPrimaryKey(QrtzPausedTriggerGrps record);
}