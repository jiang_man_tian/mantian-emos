package com.emos.wx.dao;

import com.emos.wx.bean.QrtzJobDetails;
import org.springframework.stereotype.Repository;

@Repository
public interface QrtzJobDetailsDao {

    int deleteByPrimaryKey(Integer id);

    int insert(QrtzJobDetails record);

    int insertSelective(QrtzJobDetails record);

    QrtzJobDetails selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(QrtzJobDetails record);

    int updateByPrimaryKey(QrtzJobDetails record);
}