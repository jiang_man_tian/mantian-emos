package com.emos.wx.dao;

import com.emos.wx.bean.QrtzSchedulerState;
import org.springframework.stereotype.Repository;

@Repository
public interface QrtzSchedulerStateDao {

    int deleteByPrimaryKey(Integer id);

    int insert(QrtzSchedulerState record);

    int insertSelective(QrtzSchedulerState record);

    QrtzSchedulerState selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(QrtzSchedulerState record);

    int updateByPrimaryKey(QrtzSchedulerState record);
}