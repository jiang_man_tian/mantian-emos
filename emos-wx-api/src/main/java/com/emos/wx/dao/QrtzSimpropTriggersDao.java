package com.emos.wx.dao;

import com.emos.wx.bean.QrtzSimpropTriggers;
import org.springframework.stereotype.Repository;

@Repository
public interface QrtzSimpropTriggersDao {

    int deleteByPrimaryKey(Integer id);

    int insert(QrtzSimpropTriggers record);

    int insertSelective(QrtzSimpropTriggers record);

    QrtzSimpropTriggers selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(QrtzSimpropTriggers record);

    int updateByPrimaryKey(QrtzSimpropTriggers record);
}