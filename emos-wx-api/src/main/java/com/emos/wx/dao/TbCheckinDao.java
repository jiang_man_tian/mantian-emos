package com.emos.wx.dao;

import com.emos.wx.bean.TbCheckin;
import org.springframework.stereotype.Repository;

@Repository
public interface TbCheckinDao {

    int deleteByPrimaryKey(Integer id);

    int insert(TbCheckin record);

    int insertSelective(TbCheckin record);

    TbCheckin selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbCheckin record);

    int updateByPrimaryKey(TbCheckin record);
}