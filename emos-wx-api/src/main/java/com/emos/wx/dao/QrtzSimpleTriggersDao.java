package com.emos.wx.dao;

import com.emos.wx.bean.QrtzSimpleTriggers;
import org.springframework.stereotype.Repository;

@Repository
public interface QrtzSimpleTriggersDao {
    int deleteByPrimaryKey(Integer id);

    int insert(QrtzSimpleTriggers record);

    int insertSelective(QrtzSimpleTriggers record);

    QrtzSimpleTriggers selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(QrtzSimpleTriggers record);

    int updateByPrimaryKey(QrtzSimpleTriggers record);
}