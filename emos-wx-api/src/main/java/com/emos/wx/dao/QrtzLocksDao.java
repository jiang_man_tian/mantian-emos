package com.emos.wx.dao;

import com.emos.wx.bean.QrtzLocks;
import org.springframework.stereotype.Repository;

@Repository
public interface QrtzLocksDao {

    int deleteByPrimaryKey(Integer id);

    int insert(QrtzLocks record);

    int insertSelective(QrtzLocks record);

    QrtzLocks selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(QrtzLocks record);

    int updateByPrimaryKey(QrtzLocks record);
}