package com.emos.wx.dao;

import com.emos.wx.bean.QrtzCalendars;
import org.springframework.stereotype.Repository;

@Repository
public interface QrtzCalendarsDao {

    int deleteByPrimaryKey(Integer id);

    int insert(QrtzCalendars record);

    int insertSelective(QrtzCalendars record);

    QrtzCalendars selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(QrtzCalendars record);

    int updateByPrimaryKey(QrtzCalendars record);
}