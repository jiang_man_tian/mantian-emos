package com.emos.wx.dao;

import com.emos.wx.bean.QrtzCronTriggers;
import org.springframework.stereotype.Repository;

@Repository
public interface QrtzCronTriggersDao {

    int deleteByPrimaryKey(Integer id);

    int insert(QrtzCronTriggers record);

    int insertSelective(QrtzCronTriggers record);

    QrtzCronTriggers selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(QrtzCronTriggers record);

    int updateByPrimaryKey(QrtzCronTriggers record);
}