package com.emos.wx.bean;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * tb_holidays
 * @author 
 */
@Data
public class QrtzSimpleTriggers implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 日期
     */
    private Date date;

    private static final long serialVersionUID = 1L;
}