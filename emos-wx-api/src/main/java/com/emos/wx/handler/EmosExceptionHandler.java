package com.emos.wx.handler;

import com.alibaba.fastjson.JSON;
import com.emos.wx.utils.R;
import com.emos.wx.utils.ResultCode;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@ControllerAdvice
public class EmosExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public void exceptionProcessd(HttpServletResponse response,Exception e){
        String message = e.getLocalizedMessage();
        Map res = R.error(ResultCode.RC500.val(),message);
        String jsonStr = JSON.toJSONString(res);
        try {
            response.getOutputStream().write(jsonStr.getBytes(),0,jsonStr.length());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
